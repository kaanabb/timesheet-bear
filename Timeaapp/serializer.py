from rest_framework import serializers
from Timeaapp.models import Timesheet, Employee, Customer, Project, Effort

class TimesheetSerializer(serializers.ModelSerializer):
    class Meta:
        model=Timesheet
        fields=('employee','customer','project','efor','activity','desciption')

class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model=Employee
        fields=('name','surname')

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model=Customer
        fields=('customer_name')

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model=Project
        fields=('project_name')

class EffortSerializer(serializers.ModelSerializer):
    class Meta:
        model=Effort
        fields=('date', 'hour')        