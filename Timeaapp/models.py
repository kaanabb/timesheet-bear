from django.db import models

# Create your models here.

class Employee(models.Model):
    name= models.CharField(max_length=20)
    surname= models.CharField(max_length=20)

    def __str__(self):
        return '%s %s' % (self.name, self.surname)
    
class Effort(models.Model):
    date= models.DateField()
    hour= models.FloatField()
    def __str__(self):
        return '%s / %s' % (self.date, self.hour)


class Customer(models.Model):
    customer_name= models.CharField(max_length=20)
    def __str__(self):
        return '%s' % (self.customer_name)


class Project(models.Model):
    project_name= models.CharField(max_length=30)
    def __str__(self):
        return '%s' % (self.project_name)
    

class Timesheet (models.Model):
    employee= models.ForeignKey(Employee, on_delete=models.SET)
    customer= models.ForeignKey(Customer, on_delete=models.CASCADE)
    project= models.ForeignKey(Project, on_delete=models.CASCADE)
    efor= models.ForeignKey(Effort, on_delete=models.CASCADE)
    activity= models.CharField(max_length=255)
    desciption= models.CharField(max_length=255)

