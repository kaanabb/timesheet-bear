from django.contrib import admin

# Register your models here.

from .models import Timesheet, Employee, Customer, Project, Effort

admin.site.register(Timesheet)
admin.site.register(Employee)
admin.site.register(Customer)
admin.site.register(Project)
admin.site.register(Effort)
