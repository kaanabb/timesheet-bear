from django.shortcuts import render
#from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from Timeaapp.models import  Timesheet, Employee, Customer, Project, Effort
from Timeaapp.serializer import TimesheetSerializer, EmployeeSerializer
from rest_framework.parsers import JSONParser

# Create your views here.

def get_timesheet(request,id=0):
    if request.method=='GET':
        timesheets= Timesheet.objects.all()
        timesheet_Serializer = TimesheetSerializer(timesheets, many=True)
        return JsonResponse(timesheet_Serializer.data,safe=False)
        
    
